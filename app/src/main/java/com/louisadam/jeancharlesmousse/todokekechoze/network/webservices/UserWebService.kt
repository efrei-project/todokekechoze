package com.louisadam.jeancharlesmousse.todokekechoze.network.webservices

import com.louisadam.jeancharlesmousse.todokekechoze.authentication.LoginForm
import com.louisadam.jeancharlesmousse.todokekechoze.authentication.LoginResponse
import com.louisadam.jeancharlesmousse.todokekechoze.authentication.SignUpForm
import com.louisadam.jeancharlesmousse.todokekechoze.userinfo.UserInfo
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*

interface UserWebService {
    @Multipart
    @PATCH("users/update_avatar")
    suspend fun updateAvatar(@Part avatar: MultipartBody.Part): Response<UserInfo>

    @POST("users/sign_up")
    suspend fun createUser(@Body signUpForm: SignUpForm): Response<LoginResponse>

    @POST("users/login")
    suspend fun login(@Body user: LoginForm): Response<LoginResponse>

    @GET("users/info")
    suspend fun getUser(): Response<UserInfo>

    @PATCH("users")
    suspend fun updateUser(@Body userInfo: UserInfo): Response<UserInfo>

    @DELETE("users/{mail}")
    suspend fun deleteUser(@Path("mail") mail: String?): Response<String>

}