package com.louisadam.jeancharlesmousse.todokekechoze.authentication

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController

import com.louisadam.jeancharlesmousse.todokekechoze.R
import com.louisadam.jeancharlesmousse.todokekechoze.databinding.FragmentSignupBinding
import com.louisadam.jeancharlesmousse.todokekechoze.network.Api
import com.louisadam.jeancharlesmousse.todokekechoze.userinfo.UserInfoViewModel
import com.louisadam.jeancharlesmousse.todokekechoze.utils.displayErrorToast

class SignUpFragment : Fragment() {
    private lateinit var binding: FragmentSignupBinding
    private val viewModel by lazy {
        ViewModelProvider(this).get(UserInfoViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_signup, container, false
        )
        binding.signupSubmit.setOnClickListener {
            onSubmit()
        }
        return binding.root
    }

    private fun onSubmit() {
        val password = binding.signupPassword.text.toString().trim()
        val passwordConf = binding.signupPasswordConfirmation.text.toString().trim()

        if (password != passwordConf) {
            val toast = Toast.makeText(context, "The password doesn't match", Toast.LENGTH_SHORT)
            toast.view.setBackgroundColor(Color.RED)
            toast.view.findViewById<TextView>(android.R.id.message).setTextColor(Color.WHITE)
            toast.show()
            return
        }
        val email = binding.signupEmail.text.toString().trim()
        val firstName = binding.signupFirstname.text.toString().trim()
        val lastName = binding.signupLastname.text.toString().trim()
        val signUpForm = SignUpForm(email, firstName, lastName, password, passwordConf)

        viewModel.loginResponse.observe(viewLifecycleOwner, Observer { response ->
            response?.let {
                Api.INSTANCE.setToken(it.token)
                findNavController().navigate(SignUpFragmentDirections.actionSignupFragmentToMainActivity())
                activity?.finish()
            } ?: run {
                displayErrorToast(context, "Signup failed :/")
            }
        })
        viewModel.createUser(signUpForm)
    }

}
