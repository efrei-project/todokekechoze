package com.louisadam.jeancharlesmousse.todokekechoze.authentication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.louisadam.jeancharlesmousse.todokekechoze.R
import com.louisadam.jeancharlesmousse.todokekechoze.databinding.FragmentAuthenticationBinding
import com.louisadam.jeancharlesmousse.todokekechoze.network.Api

class AuthenticationFragment : Fragment() {
    private lateinit var binding: FragmentAuthenticationBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_authentication, container, false
        )
        binding.signup.setOnClickListener {
            findNavController().navigate(AuthenticationFragmentDirections.authenticationFragmentToSignupFragment())
        }
        binding.login.setOnClickListener {
            findNavController().navigate(AuthenticationFragmentDirections.authenticationFragmentToLoginFragment())
        }
        return binding.root
    }
}
