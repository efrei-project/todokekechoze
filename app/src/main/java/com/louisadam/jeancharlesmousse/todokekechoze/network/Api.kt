package com.louisadam.jeancharlesmousse.todokekechoze.network

import android.content.Context
import androidx.core.content.edit
import com.louisadam.jeancharlesmousse.todokekechoze.SHARED_PREF_FILE
import com.louisadam.jeancharlesmousse.todokekechoze.SHARED_PREF_TOKEN_KEY
import com.louisadam.jeancharlesmousse.todokekechoze.network.webservices.TasksWebService
import com.louisadam.jeancharlesmousse.todokekechoze.network.webservices.UserWebService
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


class Api(private val context: Context) {

    companion object {
        private const val BASE_URL = "https://android-tasks-api.herokuapp.com/api/"
        lateinit var INSTANCE: Api
    }

    private val moshi = Moshi.Builder().build()
    private val okHttpClient by lazy {
        OkHttpClient.Builder()
            .addInterceptor { chain ->
                val newRequest = chain.request().newBuilder()
                    .addHeader("Authorization", "Bearer ${getToken()}")
                    .build()
                chain.proceed(newRequest)
            }
            .build()
    }
    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()

    val tasksWebService: TasksWebService by lazy {
        retrofit.create(TasksWebService::class.java)
    }
    val userWebService: UserWebService by lazy {
        retrofit.create(UserWebService::class.java)
    }

    fun setToken(token: String?) {
        context.getSharedPreferences(SHARED_PREF_FILE, 0).edit {
            putString(SHARED_PREF_TOKEN_KEY, token)
        }
    }

    fun hasToken(): Boolean {
        return getToken().isNotEmpty()
    }

    private fun getToken(): String {
        return context.getSharedPreferences(SHARED_PREF_FILE, 0).getString(
            SHARED_PREF_TOKEN_KEY,
            ""
        )!!
    }
}
