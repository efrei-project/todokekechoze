package com.louisadam.jeancharlesmousse.todokekechoze.network.webservices

import com.louisadam.jeancharlesmousse.todokekechoze.task.Task
import retrofit2.Response
import retrofit2.http.*

interface TasksWebService {

    @POST("tasks")
    suspend fun createTask(@Body task: Task): Response<Task>

    @GET("tasks")
    suspend fun getTasks(): Response<List<Task>>

    @PATCH("tasks/{id}")
    suspend fun updateTask(@Body task: Task, @Path("id") id: String? = task.id): Response<Task>

    @DELETE("tasks/{id}")
    suspend fun deleteTask(@Path("id") id: String?): Response<String>

}