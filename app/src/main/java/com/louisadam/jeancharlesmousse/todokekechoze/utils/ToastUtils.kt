package com.louisadam.jeancharlesmousse.todokekechoze.utils

import android.content.Context
import android.graphics.Color
import android.widget.TextView
import android.widget.Toast

fun displayErrorToast(context: Context?, message: String) {
    val toast = Toast.makeText(context, message, Toast.LENGTH_SHORT)
    toast.view.setBackgroundColor(Color.RED)
    toast.view.findViewById<TextView>(android.R.id.message).setTextColor(Color.WHITE)
    toast.show()
}