package com.louisadam.jeancharlesmousse.todokekechoze.network.repository

import com.louisadam.jeancharlesmousse.todokekechoze.authentication.LoginForm
import com.louisadam.jeancharlesmousse.todokekechoze.authentication.LoginResponse
import com.louisadam.jeancharlesmousse.todokekechoze.authentication.SignUpForm
import com.louisadam.jeancharlesmousse.todokekechoze.network.Api
import com.louisadam.jeancharlesmousse.todokekechoze.userinfo.UserInfo
import okhttp3.MultipartBody

class UserInfoRepository {
    private val usersWebService = Api.INSTANCE.userWebService

    suspend fun loadUser(): UserInfo? {
        val usersResponse = usersWebService.getUser()
        return if (usersResponse.isSuccessful) usersResponse.body() else null
    }

    suspend fun createUser(signUpForm: SignUpForm): LoginResponse? {
        val usersResponse = usersWebService.createUser(signUpForm)
        return usersResponse.body()
    }

    suspend fun updateUser(user: UserInfo): UserInfo? {
        val usersResponse = usersWebService.updateUser(user)
        return usersResponse.body()
    }

    suspend fun updateAvatar(body: MultipartBody.Part): UserInfo? {
        val usersResponse = usersWebService.updateAvatar(body)
        return usersResponse.body()
    }

    suspend fun login(loginForm: LoginForm): LoginResponse? {
        val loginResponse = usersWebService.login(loginForm)

        return loginResponse.body()
    }
}