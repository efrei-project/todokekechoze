package com.louisadam.jeancharlesmousse.todokekechoze.network.repository

import com.louisadam.jeancharlesmousse.todokekechoze.network.Api
import com.louisadam.jeancharlesmousse.todokekechoze.task.Task

class TasksRepository {
    private val tasksWebService = Api.INSTANCE.tasksWebService

    suspend fun loadTasks(): List<Task>? {
        val tasksResponse = tasksWebService.getTasks()
        return if (tasksResponse.isSuccessful) tasksResponse.body() else null
    }

    suspend fun createTask(task: Task): Boolean {
        val tasksResponse = tasksWebService.createTask(task)
        return tasksResponse.isSuccessful
    }

    suspend fun updateTask(task: Task): Boolean {
        val tasksResponse = tasksWebService.updateTask(task, task.id)
        return tasksResponse.isSuccessful
    }

    suspend fun deleteTask(id: String): Boolean {
        val tasksResponse = tasksWebService.deleteTask(id)
        return tasksResponse.isSuccessful
    }
}