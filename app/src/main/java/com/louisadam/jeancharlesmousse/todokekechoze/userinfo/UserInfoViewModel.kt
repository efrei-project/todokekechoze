package com.louisadam.jeancharlesmousse.todokekechoze.userinfo

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.louisadam.jeancharlesmousse.todokekechoze.authentication.LoginForm
import com.louisadam.jeancharlesmousse.todokekechoze.authentication.LoginResponse
import com.louisadam.jeancharlesmousse.todokekechoze.authentication.SignUpForm
import com.louisadam.jeancharlesmousse.todokekechoze.network.repository.UserInfoRepository
import kotlinx.coroutines.launch
import okhttp3.MultipartBody

class UserInfoViewModel : ViewModel() {
    val userInfoLiveData = MutableLiveData<UserInfo?>()
    private val repository = UserInfoRepository()
    var loginResponse = MutableLiveData<LoginResponse?>()

    fun loadInfo() {
        viewModelScope.launch {
            userInfoLiveData.postValue(repository.loadUser())
        }
    }

    fun updateAvatar(body: MultipartBody.Part) {
        viewModelScope.launch {
            userInfoLiveData.postValue(repository.updateAvatar(body))
        }
    }

    fun updateUser(user: UserInfo) {
        viewModelScope.launch {
            userInfoLiveData.postValue(repository.updateUser(user))
        }
    }

    fun loginUser(loginForm: LoginForm) {
        viewModelScope.launch {
            loginResponse.postValue(repository.login(loginForm))
        }
    }

    fun createUser(signUpForm: SignUpForm) {
        viewModelScope.launch {
            val result = repository.createUser(signUpForm)
            loginResponse.postValue(result)
        }
    }

}