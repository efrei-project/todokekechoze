package com.louisadam.jeancharlesmousse.todokekechoze.tasklist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.louisadam.jeancharlesmousse.todokekechoze.network.repository.TasksRepository
import com.louisadam.jeancharlesmousse.todokekechoze.task.Task
import kotlinx.coroutines.launch

class TaskListViewModel : ViewModel() {
    val taskListLiveData = MutableLiveData<List<Task>?>()
    val repository = TasksRepository()

    fun loadTasks() {
        viewModelScope.launch {
            val taskList = repository.loadTasks()
            taskListLiveData.postValue(taskList)
        }
    }

    fun editTask(task: Task) {
        viewModelScope.launch {
            if (repository.updateTask(task)) {
                val newList = taskListLiveData.value.orEmpty().toMutableList()
                val position = newList.indexOfFirst { it.id == task.id }
                if (position >= 0) {
                    newList[position] = task
                    taskListLiveData.postValue(newList)
                }
            }
        }
    }

    fun deleteTask(task: Task) {
        viewModelScope.launch {
            if (repository.deleteTask(task.id)) {
                val newTasks = taskListLiveData.value.orEmpty().toMutableList()
                newTasks.remove(task)
                taskListLiveData.postValue(newTasks)
            }
        }
    }

    fun addTask(task: Task) {
        viewModelScope.launch {
            if (repository.createTask(task)) {
                val newTasks = taskListLiveData.value.orEmpty().toMutableList()
                newTasks.add(task)
                taskListLiveData.postValue(newTasks)
            }
        }
    }
}