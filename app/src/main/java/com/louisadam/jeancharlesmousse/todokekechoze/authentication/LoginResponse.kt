package com.louisadam.jeancharlesmousse.todokekechoze.authentication

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LoginResponse(
    @field:Json(name = "token")
    var token: String
)
