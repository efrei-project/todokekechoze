package com.louisadam.jeancharlesmousse.todokekechoze.authentication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController

import com.louisadam.jeancharlesmousse.todokekechoze.R
import com.louisadam.jeancharlesmousse.todokekechoze.databinding.FragmentLoginBinding
import com.louisadam.jeancharlesmousse.todokekechoze.network.Api
import com.louisadam.jeancharlesmousse.todokekechoze.userinfo.UserInfoViewModel
import com.louisadam.jeancharlesmousse.todokekechoze.utils.displayErrorToast

class LoginFragment : Fragment() {
    private lateinit var binding: FragmentLoginBinding
    private val viewModel by lazy {
        ViewModelProvider(this).get(UserInfoViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_login, container, false
        )
        binding.loginButton.setOnClickListener {
            onLogin()
        }
        return binding.root
    }

    private fun onLogin() {
        val email = binding.loginTextEmail.text.toString().trim()
        val password = binding.loginTextPassword.text.toString().trim()

        if (email.isEmpty() || password.isEmpty()) {
            displayErrorToast(context, "You have to fill all text field")
            return
        }

        viewModel.loginResponse.observe(viewLifecycleOwner, Observer { response ->
                response?.let {
                    Api.INSTANCE.setToken(it.token)
                    findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToMainActivity())
                    activity?.finish()
                } ?: run {
                    displayErrorToast(context, "Wrong identification, check your password / email")
                }
            })
        viewModel.loginUser(LoginForm(email, password))
    }
}
