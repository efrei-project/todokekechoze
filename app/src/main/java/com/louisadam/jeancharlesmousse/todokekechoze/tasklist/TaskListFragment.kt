package com.louisadam.jeancharlesmousse.todokekechoze.tasklist

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.louisadam.jeancharlesmousse.todokekechoze.R
import com.louisadam.jeancharlesmousse.todokekechoze.databinding.FragmentTaskListBinding
import com.louisadam.jeancharlesmousse.todokekechoze.network.Api
import com.louisadam.jeancharlesmousse.todokekechoze.task.Task
import com.louisadam.jeancharlesmousse.todokekechoze.userinfo.UserInfoViewModel
import java.util.*

class TaskListFragment : Fragment() {
    private lateinit var binding: FragmentTaskListBinding
    private lateinit var viewAdapter: TaskListAdapter

    private val viewModel by lazy {
        ViewModelProvider(this).get(TaskListViewModel::class.java)
    }
    private val viewModelUser by lazy {
        ViewModelProvider(this).get(UserInfoViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_task_list, container, false
        )
        savedInstanceState?.let {
            it.getParcelableArrayList<Task>("list")?.toMutableList()?.let { savedTasks ->
                viewModel.taskListLiveData.postValue(savedTasks)
            }
        }
        viewModelUser.loadInfo()

        return binding.root
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelableArrayList("list", ArrayList(viewAdapter.list))
        super.onSaveInstanceState(outState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewAdapter = TaskListAdapter()
        viewAdapter.onDeleteClickListener = { viewModel.deleteTask(it) }
        viewAdapter.onEditListener = { goToTask(it) }

        viewModel.taskListLiveData.observe(viewLifecycleOwner, Observer { newList ->
            viewAdapter.list = newList.orEmpty()
            viewAdapter.notifyDataSetChanged()
        })

        viewModelUser.userInfoLiveData.observe(viewLifecycleOwner, Observer { userInfo ->
            userInfo?.let {
                binding.userinfoTitle.text = "${it.firstName} ${it.lastName}"
                Glide.with(this).load(it.avatar)
                    .apply(RequestOptions.circleCropTransform()).into(binding.userinfoImg)
            }
        })

        binding.apply {
            firstRecyclerView.layoutManager = LinearLayoutManager(activity)
            firstRecyclerView.adapter = viewAdapter

            userinfoImg.setOnClickListener {
                navigateToProfil()
            }

            addTaskBtn.setOnClickListener {
                goToTask(null)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModelUser.loadInfo()
        viewModel.loadTasks()
    }

    private fun navigateToProfil() {
        findNavController().navigate(TaskListFragmentDirections.actionTaskListFragmentToUserInfoFragment())
    }

    private fun goToTask(task: Task?) {
        val action = TaskListFragmentDirections.actionTaskListFragmentToTaskFragment(task)
        findNavController().navigate(action)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_logout) {
            Api.INSTANCE.setToken(null)
            findNavController().navigate(TaskListFragmentDirections.actionTaskListFragmentToAuthenticationActivity())
            activity?.finish()
        }
        if (item.itemId == R.id.action_pref) {

        }
        return false
    }
}