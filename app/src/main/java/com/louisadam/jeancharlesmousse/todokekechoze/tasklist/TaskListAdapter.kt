package com.louisadam.jeancharlesmousse.todokekechoze.tasklist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.louisadam.jeancharlesmousse.todokekechoze.databinding.ItemTaskBinding
import com.louisadam.jeancharlesmousse.todokekechoze.task.Task

class TaskListAdapter : RecyclerView.Adapter<TaskListAdapter.TaskViewHolder>() {
    var list: List<Task> = emptyList()
    var onDeleteClickListener: ((Task) -> Unit)? = null
    var onEditListener: ((Task) -> Unit)? = null

    inner class TaskViewHolder(val binding: ItemTaskBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Task) {
            binding.apply {
                task = item
                taskDelete.setOnClickListener { onDeleteClickListener?.invoke(item) }
                taskCell.setOnLongClickListener {
                    onEditListener?.invoke(item)
                    true
                }
                executePendingBindings()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val binding = ItemTaskBinding.inflate(LayoutInflater.from(parent.context))
        return TaskViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.bind(list[position])
    }
}