package com.louisadam.jeancharlesmousse.todokekechoze.authentication

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LoginForm(
    @field:Json(name = "email")
    var email: String,
    @field:Json(name = "password")
    var password: String
)
