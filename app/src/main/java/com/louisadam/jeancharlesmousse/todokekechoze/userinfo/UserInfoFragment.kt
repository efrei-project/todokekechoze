package com.louisadam.jeancharlesmousse.todokekechoze.userinfo

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.louisadam.jeancharlesmousse.todokekechoze.BuildConfig
import com.louisadam.jeancharlesmousse.todokekechoze.R
import com.louisadam.jeancharlesmousse.todokekechoze.databinding.FragmentUserInfoBinding
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class UserInfoFragment : Fragment() {
    private lateinit var binding: FragmentUserInfoBinding
    private lateinit var currentPhotoPath: String

    private val viewModel by lazy {
        ViewModelProvider(this).get(UserInfoViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_user_info, container, false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            takePictureButton.setOnClickListener {
                askCameraPermissionAndOpenCamera()
            }
            uploadImageButton.setOnClickListener {
                val galleryIntent = Intent(Intent.ACTION_PICK)
                galleryIntent.type = "image/*"
                startActivityForResult(galleryIntent, GALLERY_REQUEST_CODE)
            }
            userInfoEditSubmit.setOnClickListener {
                viewModel.userInfoLiveData.value?.let {
                    it.firstName = userInfoEditFirstname.text.toString()
                    it.lastName = userInfoEditLastname.text.toString()
                    it.lastName = userInfoEditLastname.text.toString()
                    viewModel.updateUser(it)
                }
            }
        }

        viewModel.userInfoLiveData.observe(viewLifecycleOwner, Observer { userInfo ->
            userInfo?.let {
                binding.userInfo = it
                Glide.with(this).load(it.avatar).apply(RequestOptions.circleCropTransform())
                    .into(binding.imageView)
            }
        })
        viewModel.loadInfo()
    }

    private fun askCameraPermissionAndOpenCamera() {
        val granted = PackageManager.PERMISSION_GRANTED
        val cameraPermision = Manifest.permission.CAMERA
        println("ASK CAMERA PERMISSION")
        if (ActivityCompat.checkSelfPermission(context!!, cameraPermision) != granted) {
            if (shouldShowRequestPermissionRationale(cameraPermision)) {
                showDialogBeforeRequest()
            } else {
                println("REQUEST CAMERA PERMISSION")
                requestCameraPermission()
            }
        } else {
            println("OPEN CAMERA")
            openCamera()
        }
    }

    private fun showDialogBeforeRequest() {
        AlertDialog.Builder(context!!).apply {
            setMessage("On a besoin de la caméra sivouplé ! 🥺")
            setPositiveButton(android.R.string.ok) { _, _ -> requestCameraPermission() }
            setCancelable(true)
            show()
        }
    }

    private fun requestCameraPermission() {
        requestPermissions(arrayOf(Manifest.permission.CAMERA), CAMERA_PERMISSION_CODE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        println("ON PERMISSION RESULT")
        if (requestCode == CAMERA_PERMISSION_CODE && grantResults.firstOrNull() == PackageManager.PERMISSION_GRANTED) {
            println("OPEN CAM IN PERM RESULT")
            openCamera()
        } else {
            Toast.makeText(
                context,
                "Si vous refusez, on peux pas prendre de photo ! 😢",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String =
            SimpleDateFormat("yyyyMMdd_HHmmss", Locale.FRANCE).format(Date())
        val storageDir: File? = activity!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

    private fun openCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(activity!!.packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        context!!,
                        BuildConfig.FILE_PROVIDER_AUTHORITY,
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE)
                }
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            viewModel.updateAvatar(imageToBody(File(currentPhotoPath)))
        }
        if (requestCode == GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            MediaStore.Images.Media.getBitmap(activity!!.contentResolver, intent?.data)?.let {
                viewModel.updateAvatar(bitmapToBody(it))
            }
        }
    }

    private fun bitmapToBody(image: Bitmap): MultipartBody.Part {
        val f = createImageFile()
        try {
            val fos = FileOutputStream(f)
            image.compress(Bitmap.CompressFormat.PNG, 100, fos)
            fos.flush()
            fos.close()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return imageToBody(f)
    }

    private fun imageToBody(file: File): MultipartBody.Part {
        val body = RequestBody.create(MediaType.parse("image/png"), file)
        return MultipartBody.Part.createFormData("avatar", file.path, body)
    }

    companion object {
        const val CAMERA_PERMISSION_CODE = 42
        const val CAMERA_REQUEST_CODE = 2001
        const val GALLERY_REQUEST_CODE = 2002
    }
}
