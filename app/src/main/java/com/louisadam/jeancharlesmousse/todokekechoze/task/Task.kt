package com.louisadam.jeancharlesmousse.todokekechoze.task

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Task(
    @field:Json(name = "id")
    val id: String,

    @field:Json(name = "title")
    var title: String,

    @field:Json(name = "description")
    var description: String? = ""
): Parcelable