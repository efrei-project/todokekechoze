package com.louisadam.jeancharlesmousse.todokekechoze

import android.app.Application
import com.louisadam.jeancharlesmousse.todokekechoze.network.Api

class App: Application(){
    override fun onCreate() {
        super.onCreate()
        Api.INSTANCE = Api(this)
    }
}