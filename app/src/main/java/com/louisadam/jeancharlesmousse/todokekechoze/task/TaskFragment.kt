package com.louisadam.jeancharlesmousse.todokekechoze.task

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.louisadam.jeancharlesmousse.todokekechoze.R
import com.louisadam.jeancharlesmousse.todokekechoze.databinding.FragmentTaskBinding
import com.louisadam.jeancharlesmousse.todokekechoze.tasklist.TaskListViewModel

import java.util.*

class TaskFragment : Fragment() {
    private lateinit var binding: FragmentTaskBinding
    private val args: TaskFragmentArgs by navArgs()
    private val taskListViewModel by lazy {
        ViewModelProvider(this).get(TaskListViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_task, container, false)

        handleArgsReceived()

        return binding.root
    }

    private fun handleArgsReceived() {
        val taskArg = args.task

        binding.apply {
            task = taskArg

            taskSubmit.setOnClickListener {
                val newTask = Task(
                    id = task?.id ?: UUID.randomUUID().toString(),
                    title = taskInputTitle.text.toString(),
                    description = taskInputDesc.text.toString()
                )

                if (task == null) {
                    taskListViewModel.addTask(newTask)
                } else {
                    taskListViewModel.editTask(newTask)
                }

                findNavController().popBackStack()
            }
        }
    }
}