package com.louisadam.jeancharlesmousse.todokekechoze.authentication

import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.louisadam.jeancharlesmousse.todokekechoze.R

import kotlinx.android.synthetic.main.activity_authentication.*

class AuthenticationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)
        setSupportActionBar(toolbar)
        val action: String? = intent?.action
        val data: Uri? = intent?.data
        println(action)
        println(data)

    }

}
